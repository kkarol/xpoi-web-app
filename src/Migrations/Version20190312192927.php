<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190312192927 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE poi_group (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE poi ADD poi_group_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE poi ADD CONSTRAINT FK_7DBB1FD687DC8256 FOREIGN KEY (poi_group_id) REFERENCES poi_group (id)');
        $this->addSql('CREATE INDEX IDX_7DBB1FD687DC8256 ON poi (poi_group_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE poi DROP FOREIGN KEY FK_7DBB1FD687DC8256');
        $this->addSql('DROP TABLE poi_group');
        $this->addSql('DROP INDEX IDX_7DBB1FD687DC8256 ON poi');
        $this->addSql('ALTER TABLE poi DROP poi_group_id');
    }
}
