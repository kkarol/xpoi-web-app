<?php

namespace App;

define('APP_HOME_PATH', "/");
define('APP_UPLOADS_PATH', APP_HOME_PATH . "uploads/");

class Globals
{
    const APP_HOME_PATH = APP_HOME_PATH;
    const APP_UPLOADS_PATH = APP_UPLOADS_PATH;
}