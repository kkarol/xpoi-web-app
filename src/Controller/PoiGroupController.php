<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Poi;

class PoiGroupController extends AbstractController
{
    /**
     * @Route("/list/{id}", name="poi_group", methods={"GET"}, options={"expose"=true})
     */
    public function index($id)
    {
        $poiRepository = $this->getDoctrine()->getRepository(Poi::class);
        $poiList = $poiRepository->findByGroupId($id);

        return $this->render('poi_group/index.html.twig', [
            'controller_name' => 'PoiGroupController',
            'poi_list' => json_encode($poiList)
        ]);
    }


}
