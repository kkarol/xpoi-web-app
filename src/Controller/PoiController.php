<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Poi;
use App\Entity\PoiGroup;
use App\Entity\PoiMarkerIcon;
use App\Entity\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class PoiController extends AbstractController
{
    /**
     * @Route("/rest/unsaved_poi", name="unsaved_poi", methods={"GET"})
     */
    public function listPoi() {
        $pois = $this->getDoctrine()->getRepository(Poi::class)->findUnsaved();

        return new JsonResponse(array(
            'type' => 'FeatureCollection',
            'features' => $pois
        ));
    }

    /**
     * @Route("/rest/last_saved_poi", name="last_saved_poi", methods={"GET"})
     */
    public function lastSaved() {
        $lastGroup = $this->getDoctrine()->getRepository(PoiGroup::class)->findLast();
        $pois = $this->getDoctrine()->getRepository(Poi::class)->findByGroupId($lastGroup);

        return new JsonResponse(array(
            'type' => 'FeatureCollection',
            'features' => $pois
        ));
    }    

    /**
     * @Route("/rest/poi/{id}", name="get_poi", methods={"GET"})
     */
    public function getPoi($id) {
        $poi = $this->getDoctrine()->getRepository(Poi::class)->find($id);

        return new JsonResponse($poi);
    }

    /**
     * @Route("/rest/poi/{id}", name="delete_poi", methods={"DELETE"}, options={"expose"=true})
     */
    public function deletePoi($id) {
        $entityManager = $this->getDoctrine()->getManager();

        $poi = $entityManager->getRepository(Poi::class)->find($id);
        
        if($poi){
            $entityManager->remove($poi);
            $entityManager->flush();    
        }

        return new JsonResponse(array(
            "error" => 0
        ));
    }    

    /**
     * @Route("/rest/poi_test", name="test_poi", methods={"GET"})
     */
    public function testPoi() {
        return new Response($this->container->get('router')->getContext()->getBaseUrl());
    }

    /**
     * @Route("/rest/poi_group", name="group_poi", methods={"POST"})
     */
    public function groupPoi(Request $request) {
        $poisToGroup = $request->request->get('poi_array');
        if(count($poisToGroup) == 0){
            return new JsonResponse(array(
                "error" => 1,
                "message" => "Brak punktów do zapisania"
            ));
        }

        $doctrine = $this->getDoctrine();
        $poiRepository = $doctrine->getRepository(Poi::class);
        $entityManager = $doctrine->getManager();

        $poiGroup = new PoiGroup();
        $entityManager->persist($poiGroup);
        $entityManager->flush();

        foreach($poisToGroup as $poiId){
            $poi = $poiRepository->find($poiId);
            if($poi){
                $poi->setPoiGroup($poiGroup);
                $entityManager->persist($poi);
            }
        }
        $entityManager->flush();
        
        return new JsonResponse(array(
            "error" => 0,
            "poi_group" => $poiGroup->getId()
        ));
    }    

    /**
     * @Route("/rest/poi", name="add_poi", methods={"POST"})
     */
    public function addPoi(Request $request) {
        //exit(\Doctrine\Common\Util\Debug::dump($data));

        // exit(\Doctrine\Common\Util\Debug::dump($request));
        // die();

        $title = $request->request->get('title');
        $latitude = $request->request->get('latitude');
        $longitude = $request->request->get('longitude');
        $description = $request->request->get('description');
        $icon = $request->request->get('poi-marker-icon');

        if(empty($title) || empty($description) || !(is_numeric($latitude) && is_numeric($longitude))){
            return new JsonResponse(array(
                "error" => 1,
                "message" => "Wprowadzone dane są nieprawidłowe"
            ));
        }

        $files = $request->files->get('photos');
        $uploadedFiles = array();

        foreach($files as $file){
            $filename = md5(uniqid()) . '.' . $file->guessExtension();
            $original_filename = $file->getClientOriginalName();
            try {
                $file->move($this->getParameter('uploads_dir'), $filename);
            } catch (FileException $e) {
                return new JsonResponse(array(
                    "error" => 1,
                    "message" => "Przesyłanie zdjęć nie powiodło się"
                ));
            }

            $uploadedFile = new UploadedFile();
            $uploadedFile->setFileName($filename);
            $uploadedFile->setOriginalFilename($original_filename);
            $uploadedFile->setCreated(new \DateTime());
            array_push($uploadedFiles, $uploadedFile);
        }

        $entityManager = $this->getDoctrine()->getManager();

        $poi = new Poi();
        $poi->setLatitude($latitude);
        $poi->setLongitude($longitude);
        $poi->setTitle($title);
        $poi->setDescription($this->fixHtmlDescriptionLines($description));

        if($icon == "custom"){
            $iconFile = $request->files->get('poi-marker-icon-image');
            $md5 = md5_file($iconFile);

            $poiMarkerIcon = $this->getDoctrine()->getRepository(PoiMarkerIcon::class)->find($md5);

            if(!$poiMarkerIcon){
                $extension = '.' . $iconFile->guessExtension();
                $filename = $md5 . $extension;
                try {
                    $iconFile->move($this->getParameter('uploads_dir'), $filename);
                } catch (FileException $e) {
                    return new JsonResponse(array(
                        "error" => 1,
                        "message" => "Przesyłanie ikony nie powiodło się"
                    ));
                }

                $poiMarkerIcon = new PoiMarkerIcon();
                $poiMarkerIcon->setFilename($md5);
                $poiMarkerIcon->setExtension($extension);
                $entityManager->persist($poiMarkerIcon);
            }
            $poi->setPoiMarkerIcon($poiMarkerIcon);
        }        

        $entityManager->persist($poi);
        $entityManager->flush();

        foreach($uploadedFiles as $uploadedFile){
            $uploadedFile->setPoi($poi);
            $entityManager->persist($uploadedFile);
        }
        $entityManager->flush();

        $entityManager->refresh($poi);
        
        return new JsonResponse(array(
            "error" => 0,
            "poi" => $poi
        ));

        // return $this->render('poi/index.html.twig', [
        //     'controller_name' => 'POIController',
        // ]);
    }



    private function fixHtmlDescriptionLines($desc){
        $desc = preg_replace('/<\/p><p>/', "<br>", $desc);
        $desc = preg_replace('/(<br><br><br>)((<br>)*)/', "</p>$2<p>", $desc);
        return $desc;
    }

}
