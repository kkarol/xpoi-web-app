<?php

namespace App\Repository;

use App\Entity\PoiGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PoiGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method PoiGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method PoiGroup[]    findAll()
 * @method PoiGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PoiGroupRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PoiGroup::class);
    }


    public function findLast(): ?PoiGroup
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    

    // /**
    //  * @return PoiGroup[] Returns an array of PoiGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PoiGroup
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
