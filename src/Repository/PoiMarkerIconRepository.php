<?php

namespace App\Repository;

use App\Entity\PoiMarkerIcon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PoiMarkerIcon|null find($id, $lockMode = null, $lockVersion = null)
 * @method PoiMarkerIcon|null findOneBy(array $criteria, array $orderBy = null)
 * @method PoiMarkerIcon[]    findAll()
 * @method PoiMarkerIcon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PoiMarkerIconRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PoiMarkerIcon::class);
    }

    // /**
    //  * @return PoiMarkerIcon[] Returns an array of PoiMarkerIcon objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PoiMarkerIcon
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
