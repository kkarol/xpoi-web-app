<?php

namespace App\Repository;

use App\Entity\Poi;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Poi|null find($id, $lockMode = null, $lockVersion = null)
 * @method Poi|null findOneBy(array $criteria, array $orderBy = null)
 * @method Poi[]    findAll()
 * @method Poi[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PoiRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Poi::class);
    }

    // /**
    //  * @return Poi[] Returns an array of Poi objects
    //  */
    public function findByGroupId($groupId)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.poiGroup = :val')
            ->setParameter('val', $groupId)
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    } 
    
    // /**
    //  * @return Poi[] Returns an array of Poi objects
    //  */
    public function findUnsaved()
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.poiGroup is NULL')
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }     

    // /**
    //  * @return Poi[] Returns an array of Poi objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Poi
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
