<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PoiMarkerIconRepository")
 */
class PoiMarkerIcon
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=32)
     */
    private $filename;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $extension;    

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getExtension(): ?string
    {
        return $this->extension;
    }

    public function setExtension(string $extension): self
    {
        $this->extension = $extension;

        return $this;
    }

    public function getFullFilename(): ?string
    {
        return $this->filename . $this->extension;
    }

}
