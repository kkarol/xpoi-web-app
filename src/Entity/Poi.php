<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\Collection;
use App\Globals;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PoiRepository")
 */
class Poi implements \JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=9, scale=7)
     */
    private $latitude;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=7)
     */
    private $longitude;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UploadedFile", mappedBy="poi", orphanRemoval=true)
     */
    private $images; 

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PoiMarkerIcon")
     * @JoinColumn(name="poi_marker_icon_id", referencedColumnName="filename")
     */
    private $poiMarkerIcon;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PoiGroup")
     * @JoinColumn(name="poi_group_id", referencedColumnName="id")
     */
    private $poiGroup;


    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function setLatitude($latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function setLongitude($longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|UploadedFile[]
     */    
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function getPoiMarkerIcon(): ?PoiMarkerIcon
    {
        return $this->poiMarkerIcon;
    }

    public function setPoiMarkerIcon(?PoiMarkerIcon $poiMarkerIcon): self
    {
        $this->poiMarkerIcon = $poiMarkerIcon;

        return $this;
    }

    public function getPoiGroup(): ?PoiGroup
    {
        return $this->poiGroup;
    }

    public function setPoiGroup(?PoiGroup $poiGroup): self
    {
        $this->poiGroup = $poiGroup;

        return $this;
    }

    public function getPoiMarkerIconUrl(): ?string
    {
        $pm = $this->getPoiMarkerIcon();
        if(empty($pm)){
            return "";
        }
        else{
            return Globals::APP_UPLOADS_PATH . $this->getPoiMarkerIcon()->getFullFilename();
        }
    }

    public function getImageFiles(): array
    {
        $images = $this->getImages();
        $arr = array();
        foreach($images as $image){
            array_push($arr, Globals::APP_UPLOADS_PATH . $image->getFilename());
        }
        return $arr;
    }

    public function jsonSerialize(): array
    {
        return [
            'type' => 'Feature',
            'geometry' => array(
                'type' => 'Point',
                'coordinates' => array(floatval($this->longitude), floatval($this->latitude))
            ),
            'properties' =>array(
                'id' => $this->id,
                'title' => $this->title,
                'description' => $this->description,
                'images' => $this->getImageFiles(),
                'marker_icon' => $this->getPoiMarkerIconUrl()
            )
        ];
    }
}
