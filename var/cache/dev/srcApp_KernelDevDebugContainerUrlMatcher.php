<?php

use Symfony\Component\Routing\Matcher\Dumper\PhpMatcherTrait;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcApp_KernelDevDebugContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    use PhpMatcherTrait;

    public function __construct(RequestContext $context)
    {
        $this->context = $context;
        $this->staticRoutes = array(
            '/' => array(array(array('_route' => 'home', '_controller' => 'App\\Controller\\HomeController::index'), null, null, null, false, null)),
            '/rest/unsaved_poi' => array(array(array('_route' => 'unsaved_poi', '_controller' => 'App\\Controller\\PoiController::listPoi'), null, array('GET' => 0), null, false, null)),
            '/rest/last_saved_poi' => array(array(array('_route' => 'last_saved_poi', '_controller' => 'App\\Controller\\PoiController::lastSaved'), null, array('GET' => 0), null, false, null)),
            '/rest/poi_test' => array(array(array('_route' => 'test_poi', '_controller' => 'App\\Controller\\PoiController::testPoi'), null, array('GET' => 0), null, false, null)),
            '/rest/poi_group' => array(array(array('_route' => 'group_poi', '_controller' => 'App\\Controller\\PoiController::groupPoi'), null, array('POST' => 0), null, false, null)),
            '/rest/poi' => array(array(array('_route' => 'add_poi', '_controller' => 'App\\Controller\\PoiController::addPoi'), null, array('POST' => 0), null, false, null)),
            '/_profiler' => array(array(array('_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'), null, null, null, true, null)),
            '/_profiler/search' => array(array(array('_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'), null, null, null, false, null)),
            '/_profiler/search_bar' => array(array(array('_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'), null, null, null, false, null)),
            '/_profiler/phpinfo' => array(array(array('_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'), null, null, null, false, null)),
            '/_profiler/open' => array(array(array('_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'), null, null, null, false, null)),
        );
        $this->regexpList = array(
            0 => '{^(?'
                    .'|/rest/poi/([^/]++)(?'
                        .'|(*:28)'
                    .')'
                    .'|/list/([^/]++)(*:50)'
                    .'|/js/routing(?:\\.(js|json))?(*:84)'
                    .'|/_(?'
                        .'|error/(\\d+)(?:\\.([^/]++))?(*:122)'
                        .'|wdt/([^/]++)(*:142)'
                        .'|profiler/([^/]++)(?'
                            .'|/(?'
                                .'|search/results(*:188)'
                                .'|router(*:202)'
                                .'|exception(?'
                                    .'|(*:222)'
                                    .'|\\.css(*:235)'
                                .')'
                            .')'
                            .'|(*:245)'
                        .')'
                    .')'
                .')(?:/?)$}sDu',
        );
        $this->dynamicRoutes = array(
            28 => array(
                array(array('_route' => 'get_poi', '_controller' => 'App\\Controller\\PoiController::getPoi'), array('id'), array('GET' => 0), null, false, null),
                array(array('_route' => 'delete_poi', '_controller' => 'App\\Controller\\PoiController::deletePoi'), array('id'), array('DELETE' => 0), null, false, null),
            ),
            50 => array(array(array('_route' => 'poi_group', '_controller' => 'App\\Controller\\PoiGroupController::index'), array('id'), array('GET' => 0), null, false, null)),
            84 => array(array(array('_route' => 'fos_js_routing_js', '_controller' => 'fos_js_routing.controller::indexAction', '_format' => 'js'), array('_format'), array('GET' => 0), null, false, null)),
            122 => array(array(array('_route' => '_twig_error_test', '_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'), array('code', '_format'), null, null, false, null)),
            142 => array(array(array('_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'), array('token'), null, null, false, null)),
            188 => array(array(array('_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'), array('token'), null, null, false, null)),
            202 => array(array(array('_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'), array('token'), null, null, false, null)),
            222 => array(array(array('_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception::showAction'), array('token'), null, null, false, null)),
            235 => array(array(array('_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception::cssAction'), array('token'), null, null, false, null)),
            245 => array(array(array('_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'), array('token'), null, null, false, null)),
        );
    }
}
