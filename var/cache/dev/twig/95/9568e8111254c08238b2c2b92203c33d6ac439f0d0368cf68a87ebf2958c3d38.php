<?php

/* home/index.html.twig */
class __TwigTemplate_5c2701168c8237e889317f8e12e2055e029ca1ae604bd4d00ba7a4bcc294637b extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "home/index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "home/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "home/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Hello ";
        echo twig_escape_filter($this->env, (isset($context["controller_name"]) || array_key_exists("controller_name", $context) ? $context["controller_name"] : (function () { throw new Twig_Error_Runtime('Variable "controller_name" does not exist.', 3, $this->source); })()), "html", null, true);
        echo "!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 6
        echo "<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">
<link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/cover.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

<style>
  #map {
    height: 100%;
  }
  html, body {
    height: 100%;
    margin: 0;
    padding: 0;
  }
</style>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 21
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 22
        echo "<div class=\"d-flex w-100 flex-column\" style=\"padding: 1rem 1rem 0\">
    <header class=\"masthead\">
      <div class=\"inner\">
        <h3 class=\"masthead-brand\">xPOI</h3>
        <nav class=\"nav nav-masthead justify-content-center\">
          <a class=\"nav-link\" href=\"";
        // line 27
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home");
        echo "\">+ Dodaj nowy punkt</a>
          <!-- <a class=\"nav-link\" href=\"#\">Features</a> -->
        </nav>
      </div>
    </header>

    <div role=\"main\" class=\"container-fluid\" style=\"height: 100%; padding: 0; text-shadow: none\">

  <div class=\"row justify-content-md-center\" style=\"height: 100%\">
    <div class=\"col col-lg-3 right-menu\">
        <div class=\"list-group\">
        <a href=\"#\" class=\"list-group-item list-group-item-action flex-column align-items-start active\">
            <div class=\"d-flex w-100 justify-content-between\">
            <h5 class=\"mb-1\">List group item heading</h5>
            <small>3 days ago</small>
            </div>
            <p class=\"mb-1\">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
            <small>Donec id elit non mi porta.</small>
        </a>
        <a href=\"#\" class=\"list-group-item list-group-item-action flex-column align-items-start\">
            <div class=\"d-flex w-100 justify-content-between\">
            <h5 class=\"mb-1\">List group item heading</h5>
            <small class=\"text-muted\">3 days ago</small>
            </div>
            <p class=\"mb-1\">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
            <small class=\"text-muted\">Donec id elit non mi porta.</small>
        </a>
        <a href=\"#\" class=\"list-group-item list-group-item-action flex-column align-items-start\">
            <div class=\"d-flex w-100 justify-content-between\">
            <h5 class=\"mb-1\">List group item heading</h5>
            <small class=\"text-muted\">3 days ago</small>
            </div>
            <p class=\"mb-1\">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
            <small class=\"text-muted\">Donec id elit non mi porta.</small>
        </a>
        <a href=\"#\" class=\"list-group-item list-group-item-action flex-column align-items-start\">
            <div class=\"d-flex w-100 justify-content-between\">
            <h5 class=\"mb-1\">List group item heading</h5>
            <small class=\"text-muted\">3 days ago</small>
            </div>
            <p class=\"mb-1\">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
            <small class=\"text-muted\">Donec id elit non mi porta.</small>
        </a>
        <a href=\"#\" class=\"list-group-item list-group-item-action flex-column align-items-start\">
            <div class=\"d-flex w-100 justify-content-between\">
            <h5 class=\"mb-1\">List group item heading</h5>
            <small class=\"text-muted\">3 days ago</small>
            </div>
            <p class=\"mb-1\">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
            <small class=\"text-muted\">Donec id elit non mi porta.</small>
        </a>
        <a href=\"#\" class=\"list-group-item list-group-item-action flex-column align-items-start\">
            <div class=\"d-flex w-100 justify-content-between\">
            <h5 class=\"mb-1\">List group item heading</h5>
            <small class=\"text-muted\">3 days ago</small>
            </div>
            <p class=\"mb-1\">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
            <small class=\"text-muted\">Donec id elit non mi porta.</small>
        </a>    
        <a href=\"#\" class=\"list-group-item list-group-item-action flex-column align-items-start\">
            <div class=\"d-flex w-100 justify-content-between\">
            <h5 class=\"mb-1\">List group item heading</h5>
            <small class=\"text-muted\">3 days ago</small>
            </div>
            <p class=\"mb-1\">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
            <small class=\"text-muted\">Donec id elit non mi porta.</small>
        </a> 
        <a href=\"#\" class=\"list-group-item list-group-item-action flex-column align-items-start\">
            <div class=\"d-flex w-100 justify-content-between\">
            <h5 class=\"mb-1\">List group item heading</h5>
            <small class=\"text-muted\">3 days ago</small>
            </div>
            <p class=\"mb-1\">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
            <small class=\"text-muted\">Donec id elit non mi porta.</small>
        </a>                                     
        </div>
    </div>
    <div class=\"col\">
        <div class=\"map-container\" style=\"width: 100%; height: 100%; position: absolute; right: 0;\">
            <div id=\"map\"></div>
            <script>
                var map;
                function initMap() {
                    map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: -34.397, lng: 150.644},
                    zoom: 8
                    });

                    map.data.loadGeoJson('test.php');
                    //map.data.loadGeoJson('https://storage.googleapis.com/mapsdevsite/json/google.json');

                    map.data.setStyle({
                        fillColor: 'green',
                        strokeWeight: 3
                    });
                    
                    map.data.addListener('click', function(event) {
                        console.log(event);
                        map.data.overrideStyle(event.feature, {fillColor: 'red'});
                    });
                }

            </script>
            <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyCQh_PZIKPsD_dOqp7b0XqRfLyEImg0AyQ&callback=initMap\" async defer></script>
            <div id=\"infobox\">
                
            </div>
        </div>
    </div>
  </div>


    </div>

    <!-- <footer class=\"mastfoot mt-auto\">
      <div class=\"inner\">
         <p>Cover template for <a href=\"https://getbootstrap.com/\">Bootstrap</a>, by <a href=\"https://twitter.com/mdo\">@mdo</a>.</p> 
      </div>
    </footer> -->
  </div>

  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>
  <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\" integrity=\"sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49\" crossorigin=\"anonymous\"></script>
  <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js\" integrity=\"sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy\" crossorigin=\"anonymous\"></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "home/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 27,  110 => 22,  101 => 21,  78 => 7,  75 => 6,  66 => 5,  46 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block title %}Hello {{ controller_name }}!{% endblock %}

{% block stylesheets %}
<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">
<link href=\"{{ asset('css/cover.css') }}\" rel=\"stylesheet\">

<style>
  #map {
    height: 100%;
  }
  html, body {
    height: 100%;
    margin: 0;
    padding: 0;
  }
</style>
{% endblock %}

{% block body %}
<div class=\"d-flex w-100 flex-column\" style=\"padding: 1rem 1rem 0\">
    <header class=\"masthead\">
      <div class=\"inner\">
        <h3 class=\"masthead-brand\">xPOI</h3>
        <nav class=\"nav nav-masthead justify-content-center\">
          <a class=\"nav-link\" href=\"{{ path('home') }}\">+ Dodaj nowy punkt</a>
          <!-- <a class=\"nav-link\" href=\"#\">Features</a> -->
        </nav>
      </div>
    </header>

    <div role=\"main\" class=\"container-fluid\" style=\"height: 100%; padding: 0; text-shadow: none\">

  <div class=\"row justify-content-md-center\" style=\"height: 100%\">
    <div class=\"col col-lg-3 right-menu\">
        <div class=\"list-group\">
        <a href=\"#\" class=\"list-group-item list-group-item-action flex-column align-items-start active\">
            <div class=\"d-flex w-100 justify-content-between\">
            <h5 class=\"mb-1\">List group item heading</h5>
            <small>3 days ago</small>
            </div>
            <p class=\"mb-1\">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
            <small>Donec id elit non mi porta.</small>
        </a>
        <a href=\"#\" class=\"list-group-item list-group-item-action flex-column align-items-start\">
            <div class=\"d-flex w-100 justify-content-between\">
            <h5 class=\"mb-1\">List group item heading</h5>
            <small class=\"text-muted\">3 days ago</small>
            </div>
            <p class=\"mb-1\">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
            <small class=\"text-muted\">Donec id elit non mi porta.</small>
        </a>
        <a href=\"#\" class=\"list-group-item list-group-item-action flex-column align-items-start\">
            <div class=\"d-flex w-100 justify-content-between\">
            <h5 class=\"mb-1\">List group item heading</h5>
            <small class=\"text-muted\">3 days ago</small>
            </div>
            <p class=\"mb-1\">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
            <small class=\"text-muted\">Donec id elit non mi porta.</small>
        </a>
        <a href=\"#\" class=\"list-group-item list-group-item-action flex-column align-items-start\">
            <div class=\"d-flex w-100 justify-content-between\">
            <h5 class=\"mb-1\">List group item heading</h5>
            <small class=\"text-muted\">3 days ago</small>
            </div>
            <p class=\"mb-1\">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
            <small class=\"text-muted\">Donec id elit non mi porta.</small>
        </a>
        <a href=\"#\" class=\"list-group-item list-group-item-action flex-column align-items-start\">
            <div class=\"d-flex w-100 justify-content-between\">
            <h5 class=\"mb-1\">List group item heading</h5>
            <small class=\"text-muted\">3 days ago</small>
            </div>
            <p class=\"mb-1\">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
            <small class=\"text-muted\">Donec id elit non mi porta.</small>
        </a>
        <a href=\"#\" class=\"list-group-item list-group-item-action flex-column align-items-start\">
            <div class=\"d-flex w-100 justify-content-between\">
            <h5 class=\"mb-1\">List group item heading</h5>
            <small class=\"text-muted\">3 days ago</small>
            </div>
            <p class=\"mb-1\">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
            <small class=\"text-muted\">Donec id elit non mi porta.</small>
        </a>    
        <a href=\"#\" class=\"list-group-item list-group-item-action flex-column align-items-start\">
            <div class=\"d-flex w-100 justify-content-between\">
            <h5 class=\"mb-1\">List group item heading</h5>
            <small class=\"text-muted\">3 days ago</small>
            </div>
            <p class=\"mb-1\">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
            <small class=\"text-muted\">Donec id elit non mi porta.</small>
        </a> 
        <a href=\"#\" class=\"list-group-item list-group-item-action flex-column align-items-start\">
            <div class=\"d-flex w-100 justify-content-between\">
            <h5 class=\"mb-1\">List group item heading</h5>
            <small class=\"text-muted\">3 days ago</small>
            </div>
            <p class=\"mb-1\">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
            <small class=\"text-muted\">Donec id elit non mi porta.</small>
        </a>                                     
        </div>
    </div>
    <div class=\"col\">
        <div class=\"map-container\" style=\"width: 100%; height: 100%; position: absolute; right: 0;\">
            <div id=\"map\"></div>
            <script>
                var map;
                function initMap() {
                    map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: -34.397, lng: 150.644},
                    zoom: 8
                    });

                    map.data.loadGeoJson('test.php');
                    //map.data.loadGeoJson('https://storage.googleapis.com/mapsdevsite/json/google.json');

                    map.data.setStyle({
                        fillColor: 'green',
                        strokeWeight: 3
                    });
                    
                    map.data.addListener('click', function(event) {
                        console.log(event);
                        map.data.overrideStyle(event.feature, {fillColor: 'red'});
                    });
                }

            </script>
            <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyCQh_PZIKPsD_dOqp7b0XqRfLyEImg0AyQ&callback=initMap\" async defer></script>
            <div id=\"infobox\">
                
            </div>
        </div>
    </div>
  </div>


    </div>

    <!-- <footer class=\"mastfoot mt-auto\">
      <div class=\"inner\">
         <p>Cover template for <a href=\"https://getbootstrap.com/\">Bootstrap</a>, by <a href=\"https://twitter.com/mdo\">@mdo</a>.</p> 
      </div>
    </footer> -->
  </div>

  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>
  <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\" integrity=\"sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49\" crossorigin=\"anonymous\"></script>
  <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js\" integrity=\"sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy\" crossorigin=\"anonymous\"></script>
{% endblock %}", "home/index.html.twig", "G:\\wamp64\\www\\extended-poi-app\\templates\\home\\index.html.twig");
    }
}
